/** clicked in menu remove active class in container div */
const container = document.querySelector('.container');
const menu_button = document.querySelector('#menu_button');

menu_button.addEventListener("click", ()=> {
    container.classList.toggle('active')
})

const checkInnerWidth = () => {
    if(window.innerWidth <= 768) {
        container.classList.remove('active');
    } else {
        container.classList.add('active');
    }
}

checkInnerWidth();
/* windows resize less than 768px  */
window.addEventListener('resize', () => {
    checkInnerWidth();
})
